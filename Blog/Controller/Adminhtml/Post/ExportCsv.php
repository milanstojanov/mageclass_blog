<?php
/**
 * MageClass_Blog extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  MageClass
 * @package   MageClass_Blog
 * @copyright Copyright (c) 2015
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace MageClass\Blog\Controller\Adminhtml\Post;

class ExportCsv extends \Magento\Backend\App\Action
{
    /**
     * File Factory
     * 
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * Page factory
     * 
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * constructor
     * 
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_fileFactory       = $fileFactory;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Export Post grid to CSV format
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $fileName = 'posts.csv';
        /** @var \MageClass\Blog\Block\Adminhtml\Post\Grid $grid */
        $grid = $resultPage->getLayout()->getChildBlock('mageclass_blog.post.grid', 'grid.export');
        return $this->_fileFactory->create(
            $fileName,
            $grid->getCsvFile(),
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR
        );
    }
}
