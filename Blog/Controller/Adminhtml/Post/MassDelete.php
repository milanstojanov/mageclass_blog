<?php
/**
 * MageClass_Blog extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  MageClass
 * @package   MageClass_Blog
 * @copyright Copyright (c) 2015
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace MageClass\Blog\Controller\Adminhtml\Post;

class MassDelete extends \MageClass\Blog\Controller\Adminhtml\Post
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $postIds = $this->getRequest()->getParam('post_ids');
        if (!is_array($postIds)) {
            $this->messageManager->addError(__('Please Select Posts.'));
        } else {
            try {
                foreach ($postIds as $postId) {
                    /** @var \MageClass\Blog\Model\Post $post */
                    $post = $this->_postFactory->create()->load($postId);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($postIds))
                );
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('An error occurred while deleting record(s).'));
            }
        }
        $redirectResult = $this->_resultRedirectFactory->create();
        $redirectResult->setPath('mageclass_blog/*/index');
        return $redirectResult;
    }
}
