<?php
/**
 * MageClass_Blog extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  MageClass
 * @package   MageClass_Blog
 * @copyright Copyright (c) 2015
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace MageClass\Blog\Controller\Adminhtml\Post;

class Grid extends \MageClass\Blog\Controller\Adminhtml\Post\Index
{
    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        return $this;
    }
}
