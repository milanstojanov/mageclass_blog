<?php

namespace MageClass\Blog\Controller\Post;

use Magento\Framework\App\Action\Context;
use MageClass\Blog\Model\PostFactory;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

class View extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \MageClass\Blog\Model\PostFactory
     */
    protected $postFactory;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PostFactory $postFactory,
        PageFactory $resultPageFactory,
        Registry $coreRegistry
    ) {
        $this->postFactory = $postFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context);
    }
    /**
     * Loads page content
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
    	$postId = (int) $this->getRequest()->getParam('id');
    	$post = $this->postFactory->create();
        $post->load($postId);
        // TODO check if post exists and redirect to X if not.
        $this->coreRegistry->register('post', $post);
        return $this->resultPageFactory->create();
    }
}