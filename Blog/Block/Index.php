<?php
namespace MageClass\Blog\Block;

use MageClass\Blog\Model\Resource\Post\CollectionFactory as PostCollectionFactory;
use Magento\Framework\View\Element\Template\Context;


class Index extends \Magento\Framework\View\Element\Template
{
	protected $postCollectionFactory;

	public function __construct(
		PostCollectionFactory $postCollectionFactory,
		Context $context
	)
	{
		$this->postCollectionFactory = $postCollectionFactory;
        parent::__construct($context);
	}

	protected  function _construct()
    {
        parent::_construct();
    }

	public function getPosts()
	{
		return $this->postCollectionFactory->create()->addFieldToSelect('*');
	}
	public function getPostUrl($postId)
	{
		return $this->getUrl('blog/post/view', ['id' => $postId]);
	}
}