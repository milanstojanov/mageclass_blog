<?php
namespace MageClass\Blog\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;


class View extends Template
{
	/**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

	public function __construct(
        Context $context,
        Registry $registry,
    )
    {
        $this->coreRegistry = $registry;
        parent::__construct($context);
    }

	protected  function _construct()
    {
        parent::_construct();
    }

	/**
     * get current author
     *
     * @return \Sample\News\Model\Author
     */
    public function getPost()
    {
        return $this->coreRegistry->registry('post');
    }
}